<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Color;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class Image_Controller extends Controller
{
    public function destroy(Request $request)
    {
        try {
            $request->validate([
                'product_id' => 'required',
                'color_id' => 'required',
            ], [
                'product_id.required' => 'El Producto ID es obligatorio.',
                'color_id.required' => 'El Color ID es obligatorio.',
            ]);

            $product_id = $request->input('product_id');
            $color_id = $request->input('color_id');

            $images = Image::where('product_id', $product_id)->where('color_id', $color_id)->get();

            foreach ($images as $image) {
                $image->delete();
            }

            return redirect()->route('index')->with('success', 'Imagen/es eliminada/s con éxito');
        } catch (ValidationException $e) {
            return redirect()->route('index')->withErrors($e->errors())->withInput();
        } catch (\Exception $e) {
            return redirect()->route('index')->with('error', 'Error al eliminar la/s imagen/es. ' . $e);
        }
    }

    public function viewDestroy($product_id)
    {
        $colors = Image::where('product_id', $product_id)->distinct('color_id')->pluck('color_id')
            ->toArray();
        $colors = Color::whereIn('id', $colors)->get();


        return view('destroyImg', compact('product_id', 'colors'));
    }

    public function viewAdd($product_id)
    {

        $colors = Color::all();

        return view('addImage', compact('product_id', 'colors'));
    }

    public function addImageWithColor(Request $request, $product_id)
    {
        try {
            $validatedData = $request->validate([
                'image_urls' => 'required',
                'color' => 'required',
            ], [
                'image_urls.required' => 'El campo URL no puede ser nulo',
                'color.required' => 'El Campo Color no puede ser nulo'
            ]);

            $imageUrls = explode(',', $validatedData['image_urls']);
            $colorId = $validatedData['color'];

            foreach ($imageUrls as $imageUrl) {
                $image = new Image;
                $image->url = trim($imageUrl);
                $image->color_id = $colorId;
                $image->product_id = $product_id;
                $image->save();
            }

            return redirect()->route('addImage', $product_id)->with('success', 'Imágenes agregadas exitosamente');
        } catch (ValidationException $e) {
            return redirect()->route('addImage', $product_id)->withErrors($e->errors())->withInput();
        } catch (\Exception $e) {
            return redirect()->route('addImage', $product_id)->with('error', 'Error al agregar la/s imagen/es. ' . $e);
        }
    }
}
