<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Form_Product_Controller extends Controller
{
    public function list()
    {

        $colors = DB::table('color')->select('color.*')
            ->where('color.active', 1)
            ->get()->pluck('name', 'id')
            ->toArray();

        return view('formCreate', [
            'colors' => $colors
        ]);
    }
}
