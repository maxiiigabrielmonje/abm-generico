<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class User_Controller extends Controller
{
    public function create(Request $request)
    {
        try {
            DB::beginTransaction();

            $request->validate([
                'email' => 'required|max:100',
                'name' => 'required|max:20',
                'lastname' => 'required|max:50',
                'password' => 'required|min:6|max:12',
            ], [
                'email.required' => 'El email no puede ser nulo',
                'name.required' => 'El nombre no puede ser nulo',
                'name.max' => 'El nombre no puede tener mas de 20 caracteres',
                'lastname.max' => 'El apellido no puede tener mas de 50 caracteres',
                'lastname.required' => 'El apellido no puede ser nulo',
                'password.min' => 'La contraseña debe tener al menos :min caracteres.',
                'password.max' => 'La contraseña debe tener un máximo de :max caracteres.'
            ]);



            if ($request->password != $request->passwordConfirmation) {

                return redirect()->back()->withErrors(['password' => 'las contraseñas no coinciden'])->withInput($request->except('password', 'password_confirmation'));
            } else if ($request->email != $request->emailConfirmation) {
                return redirect()->back()->withErrors(['email' => 'los emails no coinciden'])->withInput($request->except('password', 'password_confirmation'));
            }



            $user = User::where('email', $request->email)->first();


            if ($user) {
                return redirect()->back()->withErrors(['email' => 'Ese correo electrónico ya está registrado'])->withInput(
                    $request->except('password', 'password_confirmation')
                );
            } else {

                $user = new User;

                $user->name = $request->name;
                $user->lastname = $request->lastname;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->active = 1;

                $user->save();
                DB::commit();

                return redirect()->route('index')->with('success', 'Se ha creado exitosamente el usuario');
            }
        } catch (ValidationException $e) {
            return redirect()->back()->withErrors($e->errors())->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', 'Error al guardar en la base de datos');
        }
    }
    public function getUserEmail($email)
    {
        //Busco el usuario y lo guardo en $user
        $user = User::where('email', $email);

        if ($user) {
            return $user;
        } else {
            return null;
        }
    }


    public function updatePassword(Request $request, $id)
    {
        // Buscar el usuario por ID
        $user = User::find($id);

        // Verificar que el usuario existe
        if (!$user) {
            return 'Usuario no encontrado';
        }

        $request->validate([
            'password' => 'required|min:6|max:12',
        ], [
            'password.min' => 'La contraseña debe tener al menos :min caracteres.',
            'password.max' => 'La contraseña debe tener un máximo de :max caracteres.'
        ]);


        // Verificar que las contraseñas coincidan
        if ($request->password != $request->passwordConfirmation) {

            return redirect()->back()->withErrors(['password' => 'Las contraseñas no coinciden.'])->withInput();
        }

        // Actualizar la contraseña
        $user->password = Hash::make($request->password);
        $user->save();



        return redirect()->route('login')->with('success', 'Contraseña actualizada con éxito');
    }



    public function listUser()
    {

        return $listUser = User::all();
    }
}
