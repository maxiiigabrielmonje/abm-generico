<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\RateLimiter;

class Login_Controller extends Controller
{
    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required|email',
                'password' => 'required',
            ], [
                'email.required' => 'El campo de correo electrónico no puede ser nulo.',
                'email.email' => 'El campo de correo electrónico debe ser un correo válido.',
                'password.required' => 'El campo de contraseña no puede ser nulo.',

            ]);

            $credentials = $request->only('email', 'password');
            $remember = $request->has('remember');

            $maxAttempts = 5; // Número máximo de intentos permitidos
            $decayMinutes = 30; // Duración del bloqueo después de alcanzar el número máximo de intentos

            // Verifica si se alcanzó el límite de intentos de inicio de sesión
            if (RateLimiter::tooManyAttempts($request->input('email'), $maxAttempts)) {
                throw ValidationException::withMessages([
                    'email' => 'Demasiados intentos de inicio de sesión. Inténtalo de nuevo en ' . $decayMinutes . ' minutos.',
                ])->status(429);
            }

            if (Auth::attempt($credentials, $remember)) {
                $request->session()->regenerate();

                // Restablece el contador de intentos de inicio de sesión después de un inicio de sesión exitoso
                RateLimiter::clear($request->input('email'));

                return redirect()->route('index')->with('success', 'Se ha iniciado sesión exitosamente');
            } else {
                // Incrementa el contador de intentos de inicio de sesión
                RateLimiter::hit($request->input('email'), $decayMinutes * 60); // Convertir minutos a segundos

                return redirect()->route('login')
                    ->withErrors(['email' => 'Usuario y/o contraseña incorrecto'])
                    ->withInput();
            }
        } catch (ValidationException $e) {
            return redirect()->route('login')
                ->withErrors($e->errors())
                ->withInput();
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('index')->with('success', 'Se ha cerrado sesión exitosamente');
    }
}
