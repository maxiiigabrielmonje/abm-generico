<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Image;
use App\Models\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;


class Product_Controller extends Controller
{
    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $request->validate([
                'name' => 'required|max:200',
                'description' => 'required|max:500',
                'measures' => 'required',
                'price' => 'required',
                'image_urls' => 'required'
            ], [
                'name.required' => 'El campo Nombre es obligatorio.',
                'name.max' => 'El campo Nombre no puede tener más de 200 caracteres.',
                'description.required' => 'El campo Descripción es obligatorio.',
                'description.max' => 'El campo Descripción no puede tener más de 500 caracteres.',
                'measures.required' => 'El campo medidas no puede ser nulo',
                'price.required' => 'El campo precio es obligatorio',
                'image_urls' => 'El campo imagenes no puede ser nulo'
            ]);


            $product = new Product;
            $product->description = $request->description;
            $product->name = $request->name;
            $product->measures = $request->measures;
            $product->price = (float)$request->price;
            $product->active = true;
            $product->save();

            $imageUrls = explode(',', $request->image_urls);

            foreach ($imageUrls as $imageUrl) {
                $image = new Image;
                $image->url = trim($imageUrl);
                $image->color_id = $request->color;
                $image->product_id = $product->id;
                $image->save();
            }

            DB::commit();

            return redirect()->route('index')->with('success', 'Producto Creado');
        } catch (ValidationException $e) {
            return redirect()->route('formCreate')->withErrors($e->errors())->withInput();
        } catch (\Exception $e) {
            DB::rollback();


            return redirect()->route('formCreate')->with('error', 'Error al guardar en la base de datos');
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            $request->validate([
                'name' => 'max:200',
                'description' => 'max:500',
            ], [
                'name.max' => 'El campo Nombre no puede tener más de 200 caracteres.',
                'description.max' => 'El campo Descripción no puede tener más de 500 caracteres.',
            ]);
            $product = Product::findOrFail($id);

            $product->name = $request->filled('name') ? $request->name : $product->name;
            $product->description = $request->filled('description') ? $request->description : $product->description;
            $product->price = $request->filled('price') ? $request->price : $product->price;
            $product->measures = $request->filled('measures') ? $request->measures : $product->measures;

            $product->save();

            DB::commit();

            return redirect()->route('index')->with('success', 'Producto actualizado');
        } catch (ValidationException $e) {

            return redirect()->back()->with('error', $e);
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with('error', 'Error al guardar en la base de datos ' . $e);
        }
    }

    public function updateView($id)
    {
        try {
            $product = Product::findOrFail($id);
            return view('formUpdateProduct', compact('product'));
        } catch (\Exception $e) {
            $errorMessage = $e->getMessage();
            return redirect()->route('index')->withErrors(['error' => 'No se pudo encontrar el Producto: ' . $errorMessage]);
        }
    }

    public function index()
    {
        $products = Product::where('active', true)->get();

        $subquery = DB::table('image')
            ->selectRaw('MIN(id) as id')
            ->groupBy('product_id');

        $images = Image::whereIn('id', $subquery)->get();
        $colors = Color::where('active', true)->get();

        return response()->json([
            'products' => $products,
            'images' => $images,
            'colors' => $colors
        ]);
    }





    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $product = Product::findOrFail($id);
            $product->active = false;
            $product->save();
            DB::commit();

            return redirect()->route('index')->with('success', 'Producto eliminado');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->route('index')->with('error', 'No se pudo eliminar el Producto');
        }
    }


    public function show($id)
    {
        try {
            $product = Product::findOrFail($id);
            $subquery = DB::table('image')
                ->selectRaw('MIN(id) as id')
                ->where('product_id', $id)
                ->groupBy('product_id');

            $images = Image::whereIn('id', $subquery)->get();

            $selectedColor = $images->first()->color_id ?? null;
            $colors = Color::all();
            return response()->json([
                'product' => $product,
                'images' => $images,
                'colors' => $colors,
                'selectedColor' => $selectedColor
            ]);
        } catch (\Exception $e) {
            $errorMessage = $e->getMessage();
            return redirect()->route('index')->withErrors(['error' => 'No se pudo encontrar el Producto: ' . $errorMessage]);
        }
    }
}
