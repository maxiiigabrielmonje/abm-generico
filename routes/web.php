<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Product_Controller;
use App\Http\Controllers\User_Controller;
use App\Http\Controllers\Login_Controller;
use App\Http\Controllers\Form_Product_Controller;
use App\Http\Controllers\Image_Controller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Ruta para el index
Route::get('/', [Product_Controller::class, 'index'])->name('index');






//Rutas para usuarios no logueados
Route::middleware('guest')->group(function () {

    //Ruta para ver el form de crear un usuario
    Route::get('/register', function () {
        return view('register');
    });

    // Ruta que te muestra el login
    Route::get('/login', function () {
        return view('login');
    });

    // Ruta para crear un usuario
    Route::post('/register/create', [User_Controller::class, 'create'])->name('userCreate');

    // Ruta para loguearte
    Route::post('/login', [Login_Controller::class, 'login'])->name('login');
});


//Rutas para usuarios logueados

Route::middleware('auth')->group(function () {

    //Ruta del logout
    Route::post('/logout', [Login_Controller::class, 'logout'])->name('logout');
});



//--IMAGENES--

//Ruta para eliminar imagenes segun el product_id y el color_id
Route::post('/images/destroy', [Image_Controller::class, 'destroy'])->name('imageDestroy');

//Ruta para mostrar el form de eliminar Imagenes
Route::get('/image/{product_id}', [Image_Controller::class, 'viewdestroy'])->name('viewdestroy');


//Ruta para el form de agregar Image
Route::get('/addImage/{product_id}', [Image_Controller::class, 'viewAdd'])->name('addImage');

//Ruta para agregar imagenes
Route::post('/addImage/{product_id}', [Image_Controller::class, 'addImageWithColor'])->name('addImage');



//--PRODUCTO--

//Ruta para el formulario de crear un producto
Route::get('/products/create', [Form_Product_Controller::class, 'list'])->name('formCreate');

// Ruta para crear producto
Route::post('/products/create', [Product_Controller::class, 'create'])->name('productCreate');

// Ruta para ver el form de update
Route::get('/products/update/{id}', [Product_Controller::class, 'updateView'])->name('formUpdate');

// Ruta para actualizar un producto
Route::put('/products/update/{id}', [Product_Controller::class, 'update'])->name('productUpdate');

// Ruta para mostrar la lista de productos
Route::get('/products', [Product_Controller::class, 'index'])->name('productIndex');

// Ruta para realizar el borrado de un producto
Route::delete('/products/destroy/{id}', [Product_Controller::class, 'destroy'])->name('productDestroy');

// Ruta para mostrar un producto
Route::get('/products/{id}', [Product_Controller::class, 'show'])->name('productShow');
