<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Color_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (color::count() === 0) {

            DB::table('color')->insert([
                'name' => 'Marrón',
                'active' => true,
            ]);

            DB::table('color')->insert([
                'name' => 'Azul',
                'active' => true,
            ]);

            DB::table('color')->insert([
                'name' => 'Negro',
                'active' => true,
            ]);

            DB::table('color')->insert([
                'name' => 'blanco',
                'active' => true,
            ]);
        }
    }
}
