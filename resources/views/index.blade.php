@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if ($errors->has('error'))
    <div class="alert alert-danger">
        {{ $errors->first('error') }}
    </div>
@endif
@if (Auth::check())
    <h1>ESTAMOS EN EL INDEX</h1>
    <form method="POST" action="{{ route('logout') }}">
        @csrf
        <button type="submit" class="btn btn-danger">Cerrar sesión</button>
    </form>
@else
    <h1>ESTAMOS EN EL INDEX</h1>
    <a href="{{ route('login') }}" class="btn btn-primary">Iniciar sesión</a>
@endif
<div class="products">
    @foreach ($products as $product)
        @php
            $firstImageFound = false;
        @endphp
        @foreach ($images as $image)
            @if ($product->id == $image->product_id && !$firstImageFound)
                <div class="card">
                    <img src="{{ $image->url }}" alt="Imagen del producto">
                    <div class="card-body">
                        <h5 class="card-title">{{ $product->name }}</h5>
                        <p class="card-text">{{ $product->description }}</p>
                        <p class="card-text">{{ $product->price }}</p>
                        @foreach ($colors as $color)
                            @if ($color->id == $image->color_id)
                                <p class="card-text">{{ $color->name }}</p>
                            @endif
                        @endforeach
                    </div>
                </div>
                @php
                    $firstImageFound = true;
                @endphp
            @endif
        @endforeach
    @endforeach
</div>
