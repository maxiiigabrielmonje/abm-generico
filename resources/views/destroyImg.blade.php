<!DOCTYPE html>
<html>

<head>
    <title>Eliminar Imágenes por ID de Producto</title>
</head>

<body>
    <h1>Eliminar Imágenes por ID de Producto</h1>
    <form action="{{ route('imageDestroy') }}" method="POST">
        @csrf
        <input type="hidden" name="product_id" value="{{ $product_id }}">
        <label for="product_id">ID de Producto:</label>
        <input type="text" id="product_id" name="product_id" value="{{ $product_id }}" readonly>

        <label for="color_id">Color:</label>
        <select id="color_id" name="color_id" required>
            @foreach ($colors as $color)
                <option value="{{ $color->id }}">{{ $color->name }}</option>
            @endforeach
        </select>

        <button type="submit">Eliminar Imágenes</button>
    </form>
</body>

</html>
