<form action="{{ route('productCreate') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div>
        <label for="name">Nombre:</label>
        <input type="text" id="name" name="name" value="{{ old('name') }}" required>
        @error('name')
            <span>{{ $message }}</span>
        @enderror
    </div>

    <div>
        <label for="description">Descripción:</label>
        <textarea id="description" name="description" required>{{ old('description') }}</textarea>
        @error('description')
            <span>{{ $message }}</span>
        @enderror
    </div>

    <div>
        <label for="measures">Medidas:</label>
        <input type="text" id="measures" name="measures" value="{{ old('measures') }}" required>
        @error('measures')
            <span>{{ $message }}</span>
        @enderror
    </div>

    <div>
        <label for="price">Precio:</label>
        <input type="number" id="price" name="price" value="{{ old('price') }}" step="any" required>
        @error('price')
            <span>{{ $message }}</span>
        @enderror
    </div>


    <div>
        <label for="image_urls">URLs de imágenes:</label>
        <h6>Cada url tiene que estar separado por una coma</h6>
        <input type="text" id="image_urls" name="image_urls" value="{{ old('image_urls') }}" required>
        @error('image_urls')
            <span>{{ $message }}</span>
        @enderror
    </div>

    <div>
        <label for="color">Color:</label>
        <select id="color" name="color" required>
            <option value="">Seleccione un color</option>
            @foreach ($colors as $id => $name)
                <option value="{{ $id }}">{{ $name }}</option>
            @endforeach
        </select>
        @error('color')
            <span>{{ $message }}</span>
        @enderror
    </div>

    <div>
        <button type="submit">Crear Producto</button>
    </div>
</form>
