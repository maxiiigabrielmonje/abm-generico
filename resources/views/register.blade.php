<form method="POST" action="{{ route('userCreate') }}">
    @csrf
    @error('error')
        <span class="text-red-500">{{ $message }}</span>
    @enderror
    <div class="form-group">
        <label for="name">Nombre:</label>
        <input type="text" name="name" id="name" class="form-control" required>
    </div>
    @error('name')
        <span class="text-red-500">{{ $message }}</span>
    @enderror


    <div class="form-group">
        <label for="lastname">Apellido:</label>
        <input type="text" name="lastname" id="lastname" class="form-control" required>
    </div>
    @error('lastname')
        <span class="text-red-500">{{ $message }}</span>
    @enderror

    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" name="email" id="email" class="form-control" required>
    </div>

    <div class="form-group">
        <label for="emailConfirmation">Confirmar Email:</label>
        <input type="email" name="emailConfirmation" id="emailConfirmation" class="form-control" required>
    </div>
    @error('email')
        <span class="text-red-500">{{ $message }}</span>
    @enderror

    <div class="form-group">
        <label for="password">Contraseña:</label>
        <input type="password" name="password" id="password" class="form-control" required>
    </div>
    @error('password')
        <span class="text-red-500">{{ $message }}</span>
    @enderror
    <div class="form-group">
        <label for="passwordConfirmation">Confirmar Contraseña:</label>
        <input type="password" name="passwordConfirmation" id="passwordConfirmation" class="form-control" required>
    </div>

    <button type="submit" class="btn btn-primary">Crear</button>
</form>
