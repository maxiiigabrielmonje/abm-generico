<form method="POST" action="{{ route('login') }}">
    @csrf
    @error('error')
        <span class="text-red-500">{{ $message }}</span>
    @enderror
    <div class="form-group">
        <label for="email">Correo electrónico:</label>
        <input type="email" name="email" id="email" class="form-control" required autofocus>
    </div>
    @error('email')
        <span class="text-red-500">{{ $message }}</span>
    @enderror


    <div class="form-group">
        <label for="password">Contraseña:</label>
        <input type="password" name="password" id="password" class="form-control" required>
    </div>
    @error('password')
        <span class="text-red-500">{{ $message }}</span>
    @enderror



    <button type="submit" class="btn btn-primary">Ingresar</button>
</form>
