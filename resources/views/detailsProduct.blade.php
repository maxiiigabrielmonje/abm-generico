<h1>{{ $product->name }}</h1>

<div>
    <p>{{ $product->description }}</p>
    <p>{{ $product->measures }}</p>
    <p>{{ $product->price }}</p>
</div>

<div class="color-select">
    <select id="color" name="color" onchange="changeColor()">
        @foreach ($colors as $color)
            @if ($images->where('color_id', $color->id)->count() > 0)
                <option value="{{ $color->id }}" {{ $selectedColor == $color->id ? 'selected' : '' }}>
                    {{ $color->name }}
                </option>
            @endif
        @endforeach
    </select>
</div>

<div class="product-images">
    @foreach ($colors as $color)
        @if ($images->where('color_id', $color->id)->count() > 0)
            <div class="image-container" id="image-{{ $color->id }}"
                style="{{ $selectedColor == $color->id ? 'display: block;' : 'display: none;' }}">
                @foreach ($images->where('color_id', $color->id) as $image)
                    <img src="{{ $image->url }}" alt="Imagen del producto">
                @endforeach
            </div>
        @endif
    @endforeach
</div>

<script>
    function changeColor() {
        var selectedColor = document.getElementById("color").value;
        var imageContainers = document.getElementsByClassName("image-container");

        // Ocultar todas las imágenes
        for (var i = 0; i < imageContainers.length; i++) {
            imageContainers[i].style.display = "none";
        }

        // Mostrar las imágenes correspondientes al color seleccionado
        var selectedImageContainer = document.getElementById("image-" + selectedColor);
        if (selectedImageContainer) {
            selectedImageContainer.style.display = "block";
        }
    }
</script>
