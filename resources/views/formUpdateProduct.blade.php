<!DOCTYPE html>
<html>

<head>
    <title>Actualizar Producto</title>
</head>

<body>
    <h1>Actualizar Producto</h1>
    @if (session('error'))
        <p style="color: red;">{{ session('error') }}</p>
    @endif
    <form action="{{ route('productUpdate', $product->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div>
            <label for="name">Nombre:</label>
            <input type="text" id="name" name="name" value="{{ old('name', $product->name) }}">
            @error('name')
                <p style="color: red;">{{ $message }}</p>
            @enderror
        </div>
        <div>
            <label for="description">Descripción:</label>
            <textarea id="description" name="description">{{ old('description', $product->description) }}</textarea>
            @error('description')
                <p style="color: red;">{{ $message }}</p>
            @enderror
        </div>
        <div>
            <label for="price">Precio:</label>
            <input type="text" id="price" name="price" value="{{ old('price', $product->price) }}">
            @error('price')
                <p style="color: red;">{{ $message }}</p>
            @enderror
        </div>
        <div>
            <label for="measures">Medidas:</label>
            <input type="text" id="measures" name="measures" value="{{ old('measures', $product->measures) }}">
            @error('measures')
                <p style="color: red;">{{ $message }}</p>
            @enderror
        </div>
        <button type="submit">Actualizar Producto</button>
    </form>
</body>

</html>
