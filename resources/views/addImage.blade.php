<form action="{{ route('addImage', $product_id) }}" method="post">
    @csrf

    <div>
        <label for="image_urls">URLs de imágenes:</label>
        <h6>Cada URL debe estar separada por una coma</h6>
        <input type="text" id="image_urls" name="image_urls" value="{{ old('image_urls') }}" required>
        @error('image_urls')
            <span>{{ $message }}</span>
        @enderror
    </div>

    <div>
        <label for="color">Color:</label>
        <select id="color" name="color" required>
            <option value="">Seleccione un color</option>
            @foreach ($colors as $color)
                <option value="{{ $color->id }}">{{ $color->name }}</option>
            @endforeach
        </select>
        @error('color')
            <span>{{ $message }}</span>
        @enderror
    </div>

    <button type="submit">Agregar Imágenes</button>
    @if (session('success'))
        <div>
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div>
            {{ session('error') }}
        </div>
    @endif

</form>
